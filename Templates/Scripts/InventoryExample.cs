using InventorySystemCore;
using InventorySystemUI;
using System;
using UnityEngine;
using UnityEngine.UI;

public class InventoryExample : MonoBehaviour
{
    [SerializeField] private InventoryExampleData exampleInfo;
    [SerializeField] private InventoryUIWindowTemplate inventoryUI;
    [SerializeField] private Text itemDescriber;
    [SerializeField] private Button addItemButton;
    [SerializeField] private Button removeItemButton;

    [SerializeField] private InventoryItem itemSelected;
    [SerializeField] private InventoryTemplate inventoryData;
    [SerializeField] private float timeToWaitBetweenClicks;
    private float currentWaitTime;
    // Start is called before the first frame update
    private void Start()
    {
        inventoryData = new InventoryTemplate("TestInventory", exampleInfo.inventorySize);
        foreach (InventoryItemTemplateSO itemTemplate in exampleInfo.itemsInInventory)
        {
            inventoryData.AddItem(new InventoryItem(itemTemplate));
        }
        inventoryUI.SetupInventory(inventoryData);
        inventoryUI.RegisterToInventoryItemSelection(SelectSlot);
        addItemButton.onClick.AddListener(() => AlterItemAmount(+1));
        removeItemButton.onClick.AddListener(() => AlterItemAmount(-1));

        SetButtonsStatus();
    }

    private void Update()
    {
        if (currentWaitTime > 0.05f)
        {
            currentWaitTime -= Time.deltaTime; ;
        }
    }

    protected void SelectSlot(IInventoryItem itemSlot)
    {
        if (currentWaitTime > 0.05f)
        {
            return;
        }
        currentWaitTime = timeToWaitBetweenClicks;

        itemSelected = (InventoryItem)itemSlot;
        SetButtonsStatus();
        if (itemSelected != null)
        {
            itemDescriber.text = itemSelected.GetDescription();
        }
    }

    private void AlterItemAmount(int value)
    {
        if (value > 0)
        {
            inventoryData.AddItem(new InventoryItem(itemSelected.Template));
        }
        else
        {
            itemSelected.ReduceItemStack(value*-1);
        }
    }

    private void SetButtonsStatus()
    {
        if (itemSelected == null)
        {
            addItemButton.interactable = false;
            removeItemButton.interactable = false;
        }
        else
        {
            addItemButton.interactable = true;
            removeItemButton.interactable = true;
        }
    }
}

[Serializable]
public class InventoryExampleData
{
    public int inventorySize;
    public InventoryItemTemplateSO[] itemsInInventory;
}