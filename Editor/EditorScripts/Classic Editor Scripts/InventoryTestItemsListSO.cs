using InventorySystemCore;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class InventoryTestItemsListSO : ScriptableObject
{
    [SerializeField]
    List<InventoryItemEditorContainer> items = new List<InventoryItemEditorContainer>();
    public List<InventoryItemEditorContainer> Items { get => items; }

    public void Save(string path)
    {
        AssetDatabase.CreateAsset(this, path);
        AssetDatabase.SaveAssets();
    }
}
[Serializable]
public class InventoryItemEditorContainer
{
    public GameObject inventoryItemContainer;
    public int inventoryItemAmont;
}
