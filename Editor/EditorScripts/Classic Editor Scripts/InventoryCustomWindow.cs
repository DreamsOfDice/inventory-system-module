using InventorySystemCore;
using InventorySystemUI;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;
using UnityEngine.UI;
namespace InventoryEditor
{
#if UNITY_2021_2_OR_NEWER
    public class InventoryCustomWindow : EditorWindow
    {
        int layoutSelected = 0;
        int inventorySize = 1;
        GameObject slotPrefab = null;
        List<GameObject> itemTemplates = new List<GameObject>();

        SerializedObject seriealizedListReferenceObject = null;
        ReorderableList inventoryEditorWindowReorderableListObject = null;
        InventoryTestItemsListSO editorHelperObject;

        //[MenuItem("Inventory Module/Inventory Configuration")]
        static void ShowWindow()
        {
            EditorWindow.GetWindow(typeof(InventoryCustomWindow)).Show();
        }
        private void OnEnable()
        {

            #region new shit
            editorHelperObject = ScriptableObject.CreateInstance<InventoryTestItemsListSO>();

            /*
             AssetDatabase.CreateAsset(editorHelperObject, "Assets/NewScripableObject.asset");
             AssetDatabase.SaveAssets();
            */
            EditorUtility.FocusProjectWindow();

            Selection.activeObject = editorHelperObject;
            #endregion
            seriealizedListReferenceObject = new SerializedObject(editorHelperObject);
            inventoryEditorWindowReorderableListObject = new ReorderableList(seriealizedListReferenceObject, seriealizedListReferenceObject.FindProperty("items"), true, true, true, true);
            inventoryEditorWindowReorderableListObject.drawHeaderCallback = (rect) => EditorGUI.LabelField(rect, "GameObjects");

            inventoryEditorWindowReorderableListObject.drawElementCallback = (Rect rect, int index, bool isActive, bool isFocused) =>
            {
                var element = inventoryEditorWindowReorderableListObject.serializedProperty.GetArrayElementAtIndex(index);
                rect.y += 2f;
                EditorGUI.LabelField(new Rect(rect.x, rect.y, rect.width / 3, EditorGUIUtility.singleLineHeight), "Test Items");
                EditorGUI.PropertyField(new Rect(rect.x + (rect.width / 3), rect.y, rect.width / 3, EditorGUIUtility.singleLineHeight), element.FindPropertyRelative("inventoryItemContainer"), GUIContent.none);
                EditorGUI.PropertyField(new Rect(rect.x + (2 * rect.width / 3), rect.y, rect.width / 3, EditorGUIUtility.singleLineHeight), element.FindPropertyRelative("inventoryItemAmont"), GUIContent.none);

            };
        }
        private void OnDestroy()
        {
            //  DestroyImmediate(editorHelperObject.gameObject);
        }

        private void OnInspectorUpdate()
        {
            Repaint();
        }
        void OnGUI()
        {
            GUILayout.BeginHorizontal();
            GUILayout.Label("Inventory Size", EditorStyles.label);
            inventorySize = EditorGUILayout.IntField(inventorySize);
            GUILayout.EndHorizontal();
            GUILayout.Space(15);

            GUILayout.BeginHorizontal();
            GUILayout.Label("Slot Prefab", EditorStyles.label);
            slotPrefab = (GameObject)EditorGUILayout.ObjectField(slotPrefab, typeof(GameObject), true);
            SlotPrefabSelectionCheck(slotPrefab);
            GUILayout.EndHorizontal();
            GUILayout.Space(25);

            seriealizedListReferenceObject.Update();
            Vector2 vec = Vector2.one * 500f;
            inventoryEditorWindowReorderableListObject.DoLayoutList();
            //list.DoList(new Rect(Vector2.zero, vec));
            seriealizedListReferenceObject.ApplyModifiedProperties();
            GUILayout.Space(inventoryEditorWindowReorderableListObject.GetHeight() + 30f);
            GUILayout.Space(10);
            // itemTemplates = (List<GameObject>)EditorGUILayout.ObjectField(slotPrefab, typeof(List<GameObject>), true);
            //Editorg

            layoutSelected = EditorGUILayout.Popup("Inventory Layout Type", layoutSelected, PopulateLayoutList());
            if (GUILayout.Button("Generate Inventory"))
            {
                GameObject inventory = new GameObject("Inventory");
                GameObject container = new GameObject("Container");
                container.transform.SetParent(inventory.transform);
                container.AddComponent<GridLayoutGroup>();
            }

        }

        string[] PopulateLayoutList()
        {
            List<string> layoutClassName = new List<string>();
            foreach (var assembly in AppDomain.CurrentDomain.GetAssemblies())
            {
                foreach (Type t in assembly.GetTypes())
                {
                    if (typeof(LayoutGroup).IsAssignableFrom(t))
                    {
                        layoutClassName.Add(t.Name);
                    }
                }
            }

            return layoutClassName.ToArray();
        }

        void SlotPrefabSelectionCheck(GameObject container)
        {
            if (slotPrefab == null)
            {
                return;
            }
            if (container.GetComponents(typeof(IInventorySlotUI)).Length == 0)
            {
                slotPrefab = null;
                Debug.LogError("Object does not have a script that implements IInventorySlotUI attached to it");
            }

        }

    }

    public class InventoryEditorWindowList : MonoBehaviour
    {
        [HideInInspector]
        public GameObject[] items = new GameObject[] { };
    }
#endif
}