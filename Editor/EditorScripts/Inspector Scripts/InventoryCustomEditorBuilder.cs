using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class InventoryCustomEditorBuilder <T> where T : ScriptableObject {
    public List<T> GetAllInstances() {
        var guids = AssetDatabase.FindAssets("t:" +
                                             typeof(T)
                                                 .Name); //FindAssets uses tags check documentation for more info
        var instances = new List<T>();
        for (var i = 0; i < guids.Length; i++) //probably could get optimized 
        {
            var path = AssetDatabase.GUIDToAssetPath(guids[i]);
            instances.Add(AssetDatabase.LoadAssetAtPath<T>(path));
        }

        return instances;
    }
}
