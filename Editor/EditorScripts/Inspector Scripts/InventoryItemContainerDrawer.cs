using System.Collections;
using System.Collections.Generic;
using System.Linq;
using InventorySystemCore;
using UnityEditor;
using UnityEngine;

namespace InventoryEditor {

    [CustomPropertyDrawer(typeof(InventoryItemContainer))]
    public class InventoryItemContainerDrawer : PropertyDrawer {
        private int _choiceIndex;
        private string[] _choices;

        private readonly Dictionary<string, InventoryItemTemplateSO> allItemTemplates =
            new Dictionary<string, InventoryItemTemplateSO>();

        private readonly InventoryCustomEditorBuilder<InventoryItemTemplateSO> editorBuilderHelper =
            new InventoryCustomEditorBuilder<InventoryItemTemplateSO>();

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
            SetupDropDown();
            var itemNameProperty = property.FindPropertyRelative("inspectorItemName");
            var itemTemplateProperty = property.FindPropertyRelative("template");
            var overideEffectsProperty = property.FindPropertyRelative("overrideItemData");
            itemTemplateProperty.objectReferenceValue =
                allItemTemplates[_choices[property.FindPropertyRelative("index").intValue]];

            var overrideCheckboxRect = new Rect(position.x, position.y + EditorGUIUtility.singleLineHeight, 100,
                GetPropertyHeight(overideEffectsProperty, label) - EditorGUIUtility.singleLineHeight);


            overideEffectsProperty.boolValue = EditorGUI.Toggle(overrideCheckboxRect, overideEffectsProperty.boolValue);

            EditorGUI.BeginChangeCheck();
            property.FindPropertyRelative("index").intValue = EditorGUI.Popup(position,
                property.FindPropertyRelative("index").intValue, _choices);

            if (EditorGUI.EndChangeCheck()) {
                itemNameProperty.stringValue = _choices[property.FindPropertyRelative("index").intValue];
                itemTemplateProperty.objectReferenceValue =
                    allItemTemplates[_choices[property.FindPropertyRelative("index").intValue]];
            }
        }

        private void SetupDropDown() {
            if (allItemTemplates.Count == 0) {
                allItemTemplates.Add("None", null);
            }

            foreach (var item in editorBuilderHelper.GetAllInstances()) {
                if (allItemTemplates.ContainsKey(item.itemName)) continue;
                allItemTemplates.Add(item.itemName, item);
            }

            _choices = allItemTemplates.Keys.ToArray();
        }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label) {
            //int lineCount = 3;
            //return EditorGUIUtility.singleLineHeight * lineCount + EditorGUIUtility.standardVerticalSpacing * (lineCount-1);
            return EditorGUIUtility.singleLineHeight * 2;
        }

    }
}