using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor.UIElements;
using UnityEngine.UIElements;
using UnityEditor;
using InventorySystemUI;
using System;
using System.Linq;
using UnityEngine.UI;
using System.IO;
using Button = UnityEngine.UIElements.Button;

namespace InventoryEditor
{
#if UNITY_2021_2_OR_NEWER

    public class InventoryUIToolkitWindow : EditorWindow
    {
        string uxmlFilePath;
        string ussFilePath;
        
        VisualElement container;
        
        DropdownField inventoryDropdown;
        DropdownField layoutDropdown;
        DropdownField slotTypeField;

        UnityEngine.UIElements.Toggle scrollableToogle;
        TextField inventoryNameField;
        ObjectField slotField;
        ObjectField canvasField;
        Button inventoryGenerationButton;

        Dictionary<string, Type> slotTypesDictionary = new Dictionary<string, Type>();
        Dictionary<string, Type> inventoryTypesDictionary = new Dictionary<string, Type>();
        Dictionary<string, Type> layoutTypesDictionary = new Dictionary<string, Type>();

        [MenuItem("Inventory Module/Inventory Toolkit Configuration")]
        static void OpenEditor()
        {
            //EditorWindow.GetWindow(typeof(InventoryUIToolkitWindow));
            InventoryUIToolkitWindow window = GetWindow<InventoryUIToolkitWindow>(GetWindow(typeof(InventoryUIToolkitWindow)));
            window.titleContent = new GUIContent("Inventory Configuration");
            window.Show();
        }

        public void CreateGUI()
        {
            FindEditorWindowFiles();
            container = rootVisualElement;
            VisualTreeAsset visualTree = AssetDatabase.LoadAssetAtPath<VisualTreeAsset>(uxmlFilePath);
            container.Add(visualTree.Instantiate());

            StyleSheet styleSheet = AssetDatabase.LoadAssetAtPath<StyleSheet>(ussFilePath);
            container.styleSheets.Add(styleSheet);

            QueryContainerForElements();
            SetupDropDowns();

            inventoryGenerationButton.clicked += GenerateInventoryObject;
        }

        private void QueryContainerForElements()
        {
            inventoryDropdown = container.Q<DropdownField>("InventoryScriptDropdown");
            layoutDropdown = container.Q<DropdownField>("LayoutGroupDropdown");
            slotTypeField = container.Q<DropdownField>("SlotUITypeDropdown");

            scrollableToogle = container.Q<UnityEngine.UIElements.Toggle>("ScrollableToogle");
            inventoryNameField = container.Q<TextField>("InventoryNameField");
            
            slotField = container.Q<ObjectField>("SlotPrefabField");
            canvasField = container.Q<ObjectField>("CanvasObjectReferenceField");
            canvasField.objectType = typeof(Canvas);
            
            inventoryGenerationButton = container.Q<UnityEngine.UIElements.Button>("InventoryGenerationButton");

        }

        private void FindEditorWindowFiles()
        {
            uxmlFilePath = Directory.GetFiles(Application.dataPath, "InventoryEditorUXML.uxml", SearchOption.AllDirectories)[0];
            string[] uxmlFilePathSplitplits = uxmlFilePath.Split('/');
            uxmlFilePath = uxmlFilePathSplitplits[uxmlFilePathSplitplits.Length - 1]; 
            
            ussFilePath = Directory.GetFiles(Application.dataPath, "InventoryEditorWindow.uss", SearchOption.AllDirectories)[0];
            string[] ussFullFilePathPortion = ussFilePath.Split('/');
            ussFilePath = ussFullFilePathPortion[ussFullFilePathPortion.Length - 1]; 

        }
        
        private void GenerateInventoryObject() {
            Canvas canvasToSpawnIn = (Canvas)canvasField.value;            
            GameObject newInventory = new GameObject(inventoryNameField.text);
            newInventory.transform.SetParent(canvasToSpawnIn.transform);            

            GameObject container = new GameObject("Container");
            container.transform.SetParent(newInventory.transform);
            
            GameObject slotContainer = new GameObject("SlotContainer");
            slotContainer.transform.SetParent(container.transform);
            
            Type typeOfInventory = inventoryTypesDictionary[inventoryDropdown.text];
            newInventory.AddComponent(typeOfInventory);
            newInventory.GetComponent<IInventoryUI>().slotPrefab = (IInventorySlotUI)slotField.value;

            container.AddComponent<UnityEngine.UI.Image>();

            Type typeOfLayout = layoutTypesDictionary[layoutDropdown.text];
            slotContainer.AddComponent(typeOfLayout);

            if (scrollableToogle.value == true) {
                SetScrollableElements(newInventory,container);
            }

        }

        private void SetupDropDowns()
        {
            inventoryDropdown.choices = PopulateLayoutList(typeof(IInventoryUI), inventoryTypesDictionary).ToList();
            inventoryDropdown.index = 0;

            layoutDropdown.choices = PopulateLayoutList(typeof(LayoutGroup),layoutTypesDictionary).ToList();
            layoutDropdown.index = 0;

            slotTypeField.choices = PopulateLayoutList(typeof(IInventorySlotUI), slotTypesDictionary).ToList();
            slotTypeField.RegisterValueChangedCallback(x => SetUISlotType());
            slotTypeField.index = 0;
            SetUISlotType();

        }

        string[] PopulateLayoutList(Type typeToSearch,Dictionary<string,Type> dictionaryToStore = null)
        {
            List<string> layoutClassName = new List<string>();
            foreach (var assembly in AppDomain.CurrentDomain.GetAssemblies())
            {
                foreach (Type t in assembly.GetTypes())
                {
                    if (typeToSearch.IsAssignableFrom(t) && !t.IsAbstract )
                    {
                        layoutClassName.Add(t.Name);
                        if (dictionaryToStore != null) {
                            dictionaryToStore.Add(t.Name, t);
                        }
                    }
                }
            }

            return layoutClassName.ToArray();
        }

        private void SetUISlotType()
        {
            Type selectedType = slotTypesDictionary[slotTypeField.text];
            slotField.objectType = selectedType;
        }      
        private void SetScrollableElements(GameObject inventory, GameObject container)
        {
            inventory.AddComponent<ScrollRect>();
            container.AddComponent<Mask>();
            container.transform.GetChild(0).gameObject.AddComponent<ContentSizeFitter>();

            GameObject scrollbar = new GameObject("Scrollbar");
            scrollbar.transform.SetParent(inventory.transform);
            scrollbar.AddComponent<Scrollbar>();
            scrollbar.AddComponent<UnityEngine.UI.Image>();
            
            GameObject slidingArea = new GameObject("Sliding Area");
            slidingArea.transform.SetParent(scrollbar.transform);
            
            GameObject handle = new GameObject("Handle");
            handle.transform.SetParent(slidingArea.transform);
            handle.AddComponent<UnityEngine.UI.Image>();

        }
    }
#endif
}