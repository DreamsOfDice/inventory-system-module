﻿using System;
using System.Collections;
using System.Collections.Generic;
using InventorySystemCore;
using InventorySystemUI;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.TestTools;

namespace InventorySystemUITests
{
    public class InventorySystemUIPlaytests
    {
        InventoryTemplate uiInventory;
        bool upkeepDone = false;
        InventoryItemTemplateSO testItem;
        /*
       [SetUp]
       public void SetUpScene() {
           SceneManager.LoadScene("InventorySystemTestScene");
           SceneManager.sceneLoaded += Upkeep;

       }

       [TearDown]
       public void CleanUp()
       {
           SceneManager.sceneLoaded -= Upkeep;
           upkeepDone = false;

       }

       public void Upkeep(Scene scene, LoadSceneMode mode)
       {
           if (scene.name != "InventorySystemTestScene")
           {
               return;
           }
           Debug.Log("I loaded");
           uiInventory = GameObject.Find("TestCanvas").transform.GetChild(0).GetComponent<InventoryTemplate>();
           testItem = Resources.Load<InventoryItemTemplate>("InventoryTestResources/TestItem");
           upkeepDone = true;

       }

       // A UnityTest behaves like a coroutine in Play Mode. In Edit Mode you can use
       // `yield return null;` to skip a frame.
       [UnityTest]
       public IEnumerator SetupInventoryUITest()
       {
           while (upkeepDone == false)
           {

           }
           InventoryTemplate testInventory = new InventoryTemplate("testbug", 5);
           uiInventory.SetupInventory(testInventory);
           Assert.AreEqual(5,uiInventory.slots.Count);
           yield return null;
       }

       [UnityTest]
       public IEnumerator CreateInventoryUIwithAnItemTest()
       {
           while (upkeepDone == false)
           {

           }
           InventoryTemplate testInventory = new InventoryTemplate("testbug", 5);
           testInventory.AddItem(new InventoryItem(testItem));
           uiInventory.SetupInventory(testInventory);
           Assert.AreEqual(testItem.icon, uiInventory.slots[0].icon.sprite);

           yield return null;
       }

       [UnityTest]
       public IEnumerator AddItemToInventoryUI()
       {
           while (upkeepDone == false)
           {

           }
           InventoryTemplate testInventory = new InventoryTemplate("testbug", 5);
           uiInventory.SetupInventory(testInventory);
           testInventory.AddItem(new InventoryItem(testItem));
           Assert.AreEqual(testItem.icon, uiInventory.slots[0].icon.sprite);

           yield return null;
       }

       [UnityTest]
       public IEnumerator RemoveItemToInventoryUI()
       {
           while (upkeepDone == false)
           {

           }
           InventoryTemplate testInventory = new InventoryTemplate("testbug", 5);
           uiInventory.SetupInventory(testInventory);
           testInventory.AddItem(new InventoryItem(testItem));
           Assert.AreEqual(testItem.icon, uiInventory.slots[0].icon.sprite);
           testInventory.RemoveItem(new InventoryItem(testItem));
           Assert.IsNull(uiInventory.slots[0].icon.sprite);
           Assert.AreEqual("",uiInventory.slots[0].amountText.text);

           yield return null;
       }

       [UnityTest]
       public IEnumerator ItemInInventoryUIIsClickedTest()
       {
           while (upkeepDone == false)
           {

           }
           InventoryTemplate testInventory = new InventoryTemplate("testbug",5);
           uiInventory.SetupInventory(testInventory);
           InventoryItem testItemInstance = new InventoryItem(testItem);
           testInventory.AddItem(testItemInstance);
           testInventory.ItemInteraction(0);

           Assert.IsTrue(uiInventory.detailsWindow.gameObject.activeSelf);
           Assert.AreEqual(testItemInstance, testInventory.GetSelectedItem());


           yield return null;
       }

       [UnityTest]
       public IEnumerator ItemInInventoryUIIsDisplayedTest()
       {
           while (upkeepDone == false)
           {

           }
           InventoryTemplate testInventory = new InventoryTemplate("testbug", 5);
           uiInventory.SetupInventory(testInventory);
           InventoryItem testItemInstance = new InventoryItem(testItem);
           testInventory.AddItem(testItemInstance);
           testInventory.ItemInteraction(0);

           Assert.IsTrue(uiInventory.detailsWindow.gameObject.activeSelf);
           Assert.AreEqual(testItemInstance.GetDescription(), uiInventory.detailsWindow.informationText.text);
           Assert.AreEqual(testItemInstance.GetSprite(), uiInventory.detailsWindow.portrait.sprite);


           yield return null;
       }

       [UnityTest]
       public IEnumerator ItemInInventoryUIDeselectsAndSelectsTest()
       {
           while (upkeepDone == false)
           {

           }
           InventoryTemplate testInventory = new InventoryTemplate("testbug", 5);
           uiInventory.SetupInventory(testInventory);
           InventoryItem testItemInstance = new InventoryItem(testItem);
           InventoryItem testItemInstance2 = new InventoryItem(testItem);

           testInventory.AddItem(testItemInstance);
           testInventory.AddItem(testItemInstance2);
           testInventory.ItemInteraction(0);
           Assert.IsTrue(uiInventory.slots[0].GetSelectedStatus());
           Assert.IsFalse(uiInventory.slots[1].GetSelectedStatus());
           testInventory.ItemInteraction(1);
           Assert.IsTrue(uiInventory.slots[1].GetSelectedStatus());
           Assert.IsFalse(uiInventory.slots[0].GetSelectedStatus());



           yield return null;
       }

       [UnityTest]
       public IEnumerator ItemInInventoryUIIsDoubleClickedTest()
       {
           while (upkeepDone == false)
           {

           }
           InventoryTemplate testInventory = new InventoryTemplate("testbug", 5);
           uiInventory.SetupInventory(testInventory);
           testInventory.AddItem(new InventoryItem(testItem));
           testInventory.ItemInteraction(0);
           try
           {
               testInventory.ItemInteraction(0);
           }
           catch (Exception launchedException)
           {
               Assert.Pass("You got an exception with message: " + launchedException.Message + " Because there is no implementation of double clicking" +
                   "which is expected");
           }
           Assert.Fail("Your doble click didnt work");


           yield return null;
       }

      */
    }
}
