﻿using System.Collections;
using System.Collections.Generic;
using InventorySystemCore;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace InventorsySystemEditorTests
{
    public class InventorySystemInventoryTests
    {
        InventoryTemplate inventoryTestBag;

        [SetUp]
        public void Setup()
        {
            inventoryTestBag = new InventoryTemplate("TestBug Inventory",5);
        }

        [TearDown]
        public void CleanUp()
        {

        }
        
        // A Test behaves as an ordinary method
        [Test]
        public void InventoryCreationTest()
        {
            InventoryTemplate inventoryTest = new InventoryTemplate("InventoryBag",12);

            Assert.AreEqual(inventoryTest.InventoryName,"InventoryBag");
            Assert.AreEqual(inventoryTest.InventorySize, 12);

        }
        /*
        [Test]
        public void InventoryAddItemTest()
        {
            InventoryItemTemplate testItem = Resources.Load<InventoryItemTemplate>("InventoryTestResources/TestItem");

            Assert.IsTrue(inventoryTestBag.AddItem(new InventoryItem(testItem)));
            Assert.AreEqual(inventoryTestBag.GetItems()[0].GetItemName(), testItem.itemName);

        }

        [Test]
        public void InventoryRemoveItemTest()
        {
            InventoryItemTemplate testItem = Resources.Load<InventoryItemTemplate>("InventoryTestResources/TestItem");

            Assert.IsTrue(inventoryTestBag.AddItem(new InventoryItem(testItem)));
            inventoryTestBag.RemoveItem(inventoryTestBag.GetItems()[0]);
            Assert.IsNull(inventoryTestBag.GetItems()[0]);

        }

        [Test]
        public void InventoryAddItemToFullInventoryTest()
        {
            InventoryTemplate inventoryTest = new InventoryTemplate("InventoryBag", 1);
            InventoryItemTemplate testItem = Resources.Load<InventoryItemTemplate>("InventoryTestResources/TestItem");
            
            Assert.IsTrue(inventoryTest.AddItem(new InventoryItem(testItem)));
            Assert.IsFalse(inventoryTest.AddItem(new InventoryItem(testItem)));

        }

        [Test]
        public void InventoryAddAndGetStackableItemTest()
        {
            InventoryTemplate inventoryTest = new InventoryTemplate("InventoryBag", 1);
            StackableItem testItem = Resources.Load<StackableItem>("InventoryTestResources/StackableItemTest");

            Assert.IsTrue(inventoryTest.AddItem(new InventoryItem(testItem)));
            Assert.IsTrue(inventoryTest.AddItem(new InventoryItem(testItem)));
            Assert.AreEqual(inventoryTest.GetItemsByName(testItem.itemName)[0].GetItemAmount(),2);

        }

        [Test]
        public void InventoryUseItemInInventory()
        {
            InventoryTemplate inventoryTest = new InventoryTemplate("InventoryBag", 1);
            InventoryItemTemplate testItem = Resources.Load<InventoryItemTemplate>("InventoryTestResources/TestItem");

            Assert.IsTrue(inventoryTest.AddItem(new InventoryItem(testItem)));
            Assert.IsTrue(inventoryTest.GetItemsByName(testItem.itemName)[0].UseItem());
            

        }
        */
    }
}
