﻿using InventorySystemCore;
using InventorySystemUI.Utils;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using TMPro;
namespace InventorySystemUI
{
    [RequireComponent(typeof(InventoryPointerController))]
    public class InventoryUISlot : MonoBehaviour, IInventorySlotUI
    {
        protected Action<IInventorySlotUI> slotClickedEventHandler;
        protected Action<IInventorySlotUI, bool> slotHoverEventHandler;

        [SerializeField] TextMeshProUGUI amountText;
        [SerializeField] Image icon;
        [SerializeField] Image backgroundImage;
        [SerializeField] Image selectionCursor;

        protected int id = -1;
        protected IInventoryItem itemInSlot;

        protected InventoryPointerController pointerController;
        public IInventoryItem ItemInSlot { get => itemInSlot; }
        public virtual object SlotID { get => id; set => id = (int)value; }

        // Start is called before the first frame update

        #region overrideable Functions
        protected virtual void Start()
        {
            pointerController = GetComponent<InventoryPointerController>();
            pointerController.RegisterToClick(SlotSelected);
            DeselectSlot();
        }

        protected virtual void SlotSelected()
        {
            if (slotClickedEventHandler != null) {
                slotClickedEventHandler.Invoke(this);
            }
        }

        protected virtual void SlotHovered(bool hoverStatus)
        {
            if (slotHoverEventHandler != null) {
                slotHoverEventHandler.Invoke(this,hoverStatus);
            }

        }

        #endregion

        #region setters and getters
        public void SetAmount(string newAmount) {
            amountText.text = newAmount;
        }

        public void SetIcon(Sprite newIcon) {
            icon.sprite = newIcon;
        }

        private void SetItemData(IInventoryItem item)
        {
            SetAmount(itemInSlot.ItemAmount.ToString());
            SetIcon(itemInSlot.ItemIcon);
        }
        #endregion

        #region UI Slot 
        //this is not solid, not a Single responsability?
        public void AddItemToSlot(IInventoryItem item)
        {
            itemInSlot = item;
            if(item == null || item.ItemAmount == 0)
            {
                EmptySlot();
                return;
            }
            SetItemData(item);
            item.RegisterToItemValueChanges(AddItemToSlot);
        }

        public void EmptySlot()
        {
            itemInSlot = null;
            amountText.text = "";
            icon.sprite = null;
        }

        public void RegisterToSlotSelection(Action<IInventorySlotUI> method)
        {
            slotClickedEventHandler -= method;
            slotClickedEventHandler += method;
        }

        public void RegisterToSlotHover(Action<IInventorySlotUI,bool> method)
        {
            slotHoverEventHandler -= method;
            slotHoverEventHandler += method;
        }

        public void EnableSlot()
        {
            gameObject.SetActive(true);
        }

        public void DisableSlot()
        {
            gameObject.SetActive(false);
        }

        public void SelectSlot()
        {
            selectionCursor.enabled = true;
        }

        public void DeselectSlot()
        {
            selectionCursor.enabled = false;
        }


        #endregion


    }
}