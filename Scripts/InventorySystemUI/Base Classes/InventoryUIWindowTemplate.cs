﻿using InventorySystemCore;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

namespace InventorySystemUI
{
    public class InventoryUIWindowTemplate : MonoBehaviour, IInventoryUI
    {
        [SerializeField]
        protected InventoryUISlot slotPrefab;
        [SerializeField]
        protected LayoutGroup grid;
        [SerializeField]
        protected int columns;
        [SerializeField]
        protected int rows;

        protected IInventory currentInventory;

        protected Action<IInventoryItem> inventorySelectionEvent;

        protected List<IInventorySlotUI> slots = new List<IInventorySlotUI>();
        protected List<IInventorySlotUI> slotPool = new List<IInventorySlotUI>();

        protected IInventorySlotUI selectedSlot;

        IInventorySlotUI IInventoryUI.slotPrefab { set => slotPrefab = (InventoryUISlot)value; }

        public virtual void SetupInventory(IInventory inventory)
        {
            currentInventory = inventory;
            RemoveUISlots();
            List<IInventoryItem> itemsInInventory = currentInventory.GetItems();
            SetupItemSlotsWithContent(itemsInInventory);
            inventory.RegisterToChangesInInventory(UpdateUIInventory);
        }

        public virtual void ShowInventory()
        {
            gameObject.SetActive(true);
        }

        public virtual void HideInventory()
        {
            gameObject.SetActive(false);
        }

        public void RegisterToInventoryItemSelection(Action<IInventoryItem> method)
        {
            inventorySelectionEvent -= method;
            inventorySelectionEvent += method;
        }

        protected virtual void SlotSelected(IInventorySlotUI slotBeingSelected)
        {
            selectedSlot?.DeselectSlot();
            selectedSlot = slotBeingSelected;
            selectedSlot.SelectSlot();

            if (inventorySelectionEvent != null)
            {
                inventorySelectionEvent.Invoke(selectedSlot.ItemInSlot);
            }
        }

        protected virtual void ItemHover(IInventorySlotUI slotHovered, bool status)
        { }

        #region Slot building code
        protected virtual void RemoveUISlots()
        {
            foreach (IInventorySlotUI slot in grid.GetComponentsInChildren<IInventorySlotUI>())
            {
                RemoveUISlot(slot);
                slotPool.Add(slot);
            }
        }

        protected virtual void RemoveUISlot(IInventorySlotUI slotToClear)
        {
            slotToClear.EmptySlot();
            slotToClear.DisableSlot();
        }

        protected virtual IInventorySlotUI BuildUISlot(IInventoryItem slotData)
        {
            IInventorySlotUI newSlot;
            if (slotPool.Count > 0)
            {
                int poolIndex = slotPool.Count - 1;
                newSlot = slotPool[poolIndex];
                slotPool.RemoveAt(poolIndex);
                newSlot.AddItemToSlot(slotData);
                newSlot.EnableSlot();

                return newSlot;
            }
            else
            {
                newSlot = Instantiate(slotPrefab, grid.transform);
                newSlot.AddItemToSlot(slotData);
            }

            newSlot.RegisterToSlotSelection(SlotSelected);
            newSlot.RegisterToSlotHover(ItemHover);

            return newSlot;
        }
        #endregion Slot building code

        #region Helper Method
        protected virtual void SetupItemSlotsWithContent(List<IInventoryItem> itemsToAdd)
        {            
                for (int i = 0; i < currentInventory.InventorySize; i++)
                {
                    IInventoryItem slotItem = null;
                    if (itemsToAdd.Count > i)
                    {
                        slotItem = itemsToAdd[i];
                    }
                    slots.Add(BuildUISlot(slotItem));
                }
            
        }

        protected virtual void UpdateUIInventory(List<IInventoryItem> itemsToAdd)
        {
            List<IInventoryItem> itemsToSet = RefreshSlots(itemsToAdd);

            foreach (IInventoryItem item in itemsToSet)
            {
                AddItemToAvailableSlots(item);
            }
        }

        protected virtual List<IInventoryItem> RefreshSlots(List<IInventoryItem> itemsToCheck)
        {
            List<IInventoryItem> newItemsFound = itemsToCheck.ToList();

            foreach (IInventorySlotUI slot in slots)
            {
                if (slot.ItemInSlot == null) {
                    continue;
                }
                bool slotFound = false;
                foreach (IInventoryItem item in itemsToCheck)
                {
                    if (item.ItemID == slot.ItemInSlot.ItemID)
                    {                        
                        slot.AddItemToSlot(item);
                        newItemsFound.Remove(item);
                        slotFound = true;
                        break;
                    }                   
                }
                if (!slotFound)
                {
                    slot.EmptySlot();
                }
            }
            return newItemsFound;
        }

        private void AddItemToAvailableSlots(IInventoryItem item)
        {
            foreach (IInventorySlotUI slot in slots)
            {
                if (slot.ItemInSlot == null)
                {
                    slot.AddItemToSlot(item);
                    break;
                }
            }
        }

        #endregion Helper Method
    }
}