using InventorySystemCore;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace InventorySystemUI
{
    public interface IInventoryUI 
    {   
        IInventorySlotUI slotPrefab { set; }
        void SetupInventory(IInventory inventory);
        void ShowInventory();
        void HideInventory();
        void RegisterToInventoryItemSelection(Action<IInventoryItem> method);
    }
}