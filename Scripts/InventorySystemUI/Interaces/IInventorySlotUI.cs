using InventorySystemCore;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace InventorySystemUI
{
    public interface IInventorySlotUI
    {
        IInventoryItem ItemInSlot { get; }
        object SlotID { get; set; }
        void AddItemToSlot(IInventoryItem item);
        void EmptySlot();
        void EnableSlot();
        void DisableSlot();
        void SelectSlot();
        void DeselectSlot();
        void RegisterToSlotSelection(Action<IInventorySlotUI> method);
        void RegisterToSlotHover(Action<IInventorySlotUI, bool> method);
        //add a deactivate interaction but leave visible

    }
}