using System;
using UnityEngine;
using UnityEngine.EventSystems;

namespace InventorySystemUI.Utils
{
    public class InventoryPointerController : MonoBehaviour, IPointerClickHandler, IPointerEnterHandler, IPointerExitHandler
    {
        event Action areaClickedEvent;
        event Action<bool> hoverEvent;

        #region registration
        public void RegisterToClick(Action method) 
        {
            areaClickedEvent -= method;
            areaClickedEvent += method;            
        }
        #endregion
        public void RegisterToHover(Action<bool> method)
        {
            hoverEvent -= method;
            hoverEvent += method;
        }
        #region UI Pointer Handlers
        public void OnPointerClick(PointerEventData eventData)
        {
            if (areaClickedEvent != null) {
                areaClickedEvent.Invoke();
            }
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            if (hoverEvent != null) {
                hoverEvent.Invoke(true);
            }
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            if (hoverEvent != null)
            {
                hoverEvent.Invoke(false);
            }
        }
        #endregion
    }
}