using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace InventorySystemCore
{
    public interface IInventory
    {
        int InventorySize  { get; }
        string InventoryName { get; }
        void AddItem(IInventoryItem itemToAdd);
        void RemoveItem(object itemID);
        void RemoveItem(IInventoryItem itemName);        
        IInventoryItem GetItem(object itemID);
        List<IInventoryItem> GetAllItemsOfName(string itemName);
        List<IInventoryItem> GetItems();
        void RegisterToChangesInInventory(Action<List<IInventoryItem>> itemsChanged);

    }
}