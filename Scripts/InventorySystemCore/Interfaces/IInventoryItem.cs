﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace InventorySystemCore {
    public interface IInventoryItem
    {
        string ItemName { get; }
        object ItemID { get; set; }//consider changing aka removing set
        bool IsStackable { get; }
        int ItemAmount { get; }
        int ItemMaxStack { get; }
        Sprite ItemIcon { get; }
        string GetDescription();
        string GetToolTip();
        void AddToItemStack(int amountToAdd);
        void ReduceItemStack(int amountToReduce);
        void RegisterToItemValueChanges(Action<IInventoryItem> method);//maybe just the stack?
    }
}