﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace InventorySystemCore
{
    public class InventoryTemplate : IInventory
    {
       
        protected string inventoryName;
        protected int inventorySize;
        protected List<IInventoryItem> itemsInInventory;
        protected int itemSelectedID = -1;

        public int InventorySize => inventorySize;

        public string InventoryName => inventoryName;

        protected event Action<List<IInventoryItem>> inventoryChangeEvent;

        public InventoryTemplate(string newInventoryName, int newInventorySize)
        {
            inventoryName = newInventoryName;
            inventorySize = newInventorySize;
            itemsInInventory = new List<IInventoryItem>();
            
        }


        #region IInventory methods

        #region Inventory contents modification methods
        public virtual void AddItem(IInventoryItem itemToAdd)
        {
            if (itemsInInventory.Count >= inventorySize) {
                Debug.Log("Inventory is Full, could not add "+ itemToAdd.ItemName);
                return;
            }
            IInventoryItem itemToStack = ItemExistsInListAndHasStackSpace(itemToAdd);
            if (itemToStack != null)
            {
                itemToStack.AddToItemStack(itemToAdd.ItemAmount);
            } else
            {
                itemsInInventory.Add(itemToAdd);
                itemToAdd.ItemID =itemToAdd.ItemName + itemsInInventory.Count.ToString();
                itemToAdd.RegisterToItemValueChanges(ItemDataChanged);
            }
            ItemInInventoryChangedEvent();
        }

        public void RemoveItem(object itemID)
        {
            IInventoryItem itemToRemove = GetItem(itemID);
            if (itemToRemove != null)
            {
                itemsInInventory.Remove(itemToRemove);
                ItemInInventoryChangedEvent();
            }
        }
            
        public void RemoveItem(IInventoryItem itemToRemove)
        {
            if (itemsInInventory.Contains(itemToRemove)) { }
            itemsInInventory.Remove(itemToRemove);
            ItemInInventoryChangedEvent();

        }
        #endregion

        public IInventoryItem GetItem(object itemID)
        {
            foreach (IInventoryItem it in itemsInInventory)
            {
                if (it.ItemID == itemID)
                {
                    return it;
                }
            }
            return null;
        }

        public List<IInventoryItem> GetAllItemsOfName(string itemName)
        {
            List<IInventoryItem> foundItems = new List<IInventoryItem>();
            foreach (IInventoryItem it in itemsInInventory)
            {
                if (it.ItemName == itemName)
                {
                    foundItems.Add(it);
                }
            }
            return foundItems;
        }

        public List<IInventoryItem> GetItems()
        {
            return itemsInInventory;
        }
        public void RegisterToChangesInInventory(Action<List<IInventoryItem>> method)
        {
            inventoryChangeEvent -= method;
            inventoryChangeEvent += method;
        }


        #endregion IInventory methods

        #region Helper Functions
        protected IInventoryItem ItemExistsInListAndHasStackSpace(IInventoryItem item)
        {
            foreach (IInventoryItem itemTocheck in GetItems())
            {
                if (itemTocheck.ItemName == item.ItemName && item.IsStackable && item.ItemAmount + itemTocheck.ItemAmount <= item.ItemMaxStack)
                {
                    return itemTocheck;
                }
            }

            return null;
        }

        protected void ReduceInventoryItem(IInventoryItem itemToReduce, int amount)
        {
            if (itemToReduce.ItemAmount > amount)
            {
                itemToReduce.ReduceItemStack(amount);
            } else
            {
                RemoveItem(itemToReduce.ItemID);
            }
            ItemInInventoryChangedEvent();
        }

        protected virtual void ItemDataChanged(IInventoryItem itemChanged) {

            if (itemChanged.ItemAmount == 0) {
                RemoveItem(itemChanged.ItemID);
                return;
            }            
        }

        protected virtual void ItemInInventoryChangedEvent()
        {
            if (inventoryChangeEvent != null)
            {
                inventoryChangeEvent.Invoke(GetItems());
            }
        }

        #endregion Helper Functions
    }
}