﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace InventorySystemCore
{
    [CreateAssetMenu(fileName = "New Stackable Item", menuName = "Inventory System/StackableItemTemplate")]
    public class StackableItem : InventoryItemTemplateSO
    {
        public bool isStackable;
        public int maxAmount;

    }
}