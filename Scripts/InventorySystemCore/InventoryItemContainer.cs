using System;
using System.Collections;
using System.Collections.Generic;
using InventorySystemCore;
using UnityEngine;

namespace InventorySystemCore {
    [Serializable]
    public class InventoryItemContainer {
        public string inspectorItemName;
        public int index;
        public InventoryItemTemplateSO template; //change this from SO
        public bool overrideItemData = false;

        public InventoryItemTemplateSO Template {
            get => template;
            set => template = value;
        }
    }
}