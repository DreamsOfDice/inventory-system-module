﻿using System;
using System.Collections;
using System.Collections.Generic;
using InventorySystemCore;
using UnityEngine;

namespace InventorySystemCore
{
    public class InventoryItem :IInventoryItem
    {
        protected Action<IInventoryItem> itemChangedEvent;

        protected InventoryItemTemplateSO template;
        protected int maxAmount;
        protected int amount;
        protected string nameOfItem;
        protected string id = "";
        protected bool stackable;
        protected Sprite icon;
        public string ItemName => nameOfItem;

        public virtual object ItemID { get => id; set => id = value.ToString(); }
        public bool IsStackable => stackable;

        public int ItemAmount => amount;

        public int ItemMaxStack => maxAmount;

        public Sprite ItemIcon => icon;

        public virtual InventoryItemTemplateSO Template { get => template;  }


        #region constructers 
        public InventoryItem(InventoryItemTemplateSO newTemplate)
        {
            BaseInitialize(newTemplate);
        }

        public InventoryItem(InventoryItemTemplateSO newTemplate, int startingAmount)
        {
            BaseInitialize(newTemplate);
            amount = startingAmount;
            
        }
        #endregion

        #region InventoryItem Specific Methods
        protected virtual void BaseInitialize(InventoryItemTemplateSO newTemplate ) {
            template = newTemplate;
            stackable = newTemplate.stackable;
            maxAmount = newTemplate.stackSize;
            amount = 1;
            nameOfItem = newTemplate.itemName;
            icon = newTemplate.icon;
        }

        protected virtual void ItemChangedEventHandling()
        {
            if (itemChangedEvent != null)
            {
                itemChangedEvent.Invoke(this);
            }

        }

        public virtual bool UseItem()
        {
            return true;
        }
        #endregion;

        #region IInventoryItem 
        public string GetDescription()
        {
            return Template.description;
        }

        public string GetToolTip()
        {
            return Template.description;
        }

        public void AddToItemStack(int amountToAdd)
        {
            amount += amountToAdd;
            if (amount > maxAmount) {
                amount = maxAmount;
            }
            ItemChangedEventHandling();

        }

        public void ReduceItemStack(int amountToReduce)
        {
            amount = amount - amountToReduce;
            if (amount < 0) {
                amount = 0;
            }
            ItemChangedEventHandling();
        }

        public void RegisterToItemValueChanges(Action<IInventoryItem> method)
        {
            itemChangedEvent -= method;
            itemChangedEvent += method;
        }
        #endregion

        
    }
}