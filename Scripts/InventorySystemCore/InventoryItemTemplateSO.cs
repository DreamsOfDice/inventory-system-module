﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace InventorySystemCore
{

    [CreateAssetMenu(fileName = "New Item", menuName = "Inventory System/ItemTemplate")]
    public class InventoryItemTemplateSO : ScriptableObject
    {
        public string itemName = "New Item";        // Item name
        public Sprite icon = null;                  // Item Icon
        public string description;
        public bool stackable;
        public int stackSize;
        
    }
}